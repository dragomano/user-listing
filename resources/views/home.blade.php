@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Добро пожаловать, {{ Auth::user()->name }}!
                    @if (Auth::user()->role_id == 1)
                        <br>Зайти в <a href="{{ route('admin') }}">админку</a>.
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
