@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Доступные функции</div>
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item"><a href="{{ route('admin.users') }}">Управление пользователями</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
