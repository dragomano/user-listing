<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'       => 'Admin',
            'role_id'    => 1,
            'email'      => 'a@a.test',
            'password'   => bcrypt('123'),
            'created_at' => Carbon::now()
        ]);

        User::create([
            'name'       => 'User',
            'role_id'    => 2,
            'email'      => 'u@u.test',
            'password'   => bcrypt('123'),
            'created_at' => Carbon::now()
        ]);
    }
}
