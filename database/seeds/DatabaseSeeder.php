<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Создадим стандартные роли
        $this->call(RolesTableSeeder::class);

        // Создадим администратора и пользователя по умолчанию
        $this->call(UsersTableSeeder::class);

        // Создадим 30 случайных пользователей
        factory(User::class, 30)->create();
    }
}
