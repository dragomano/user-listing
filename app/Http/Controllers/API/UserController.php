<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Ищем пользователей по имени
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userName  = $request->get('user_name');
        $foundData = User::select('name', 'email', 'created_at')
            ->where('name', 'LIKE', '%' . $userName . '%')
            ->get();

        return response()->json($foundData);
    }
}
